<?php
/**
 * @file
 * Implements all features export functions.
 */

/**
 * Implements hook_features_api().
 *
 * The key of this array will be the COMPONENT name used by the different
 * hooks of features like hook_features_export_options().
 *
 * Remember to mention the file in the component to include the component
 * feature functionality.
 */
function entity_view_mode_extra_features_api() {
  return array(
    'entity_view_mode' => array(
      'name' => t('Entity View Mode'),
      'default_hook' => 'entity_view_mode_defaults',
      'default_file' => FEATURES_DEFAULTS_INCLUDED,
      'feature_source' => TRUE,
      'file' => drupal_get_path('module', 'entity_view_mode_extra') . '/entity_view_mode_extra.features.inc',
    ),
  );
}

/**
 * Implements hook_features_export_options().
 *
 * Generates the checkboxes for the components that the module provides.
 *
 * Note that the HOOK part in hook_features_export_options() is
 * the COMPONENT name specified in hook_features_api().
 */
function entity_view_mode_features_export_options() {
  $options = array();

  $view_modes = variable_get('entity_view_modes');
  if (isset($view_modes)) {
    foreach ($view_modes as $entity_type => $modes) {
      foreach ($modes as $key => $view_mode) {
        $options[$entity_type . ':' . $key] = $entity_type . ':' . $key;
      }
    }
  }

  return $options;
}

/**
 * Implements hook_features_export().
 *
 * This method provides information to the .info that will
 * be generated about what components to include. The $data
 * element provides information about what components are selected.
 * 
 * The $export array contains 2 parts that will be filled by reference.
 * - dependencies in $export['dependencies'] are the module dependencies to in
 *   include in the info file
 * - the component names go into $export['entity_view_mode']
 *
 * It returns the pipe.
 * - delegates to the pipe what other components to include.
 */
function entity_view_mode_features_export($data, &$export, $module_name) {
  $export['dependencies']['features'] = 'features';
  $export['dependencies']['entity_view_mode'] = 'entity_view_mode';

  foreach ($data as $component) {
    if (_entity_view_mode_features_component_exists($component)) {
      $export['features']['entity_view_mode'][$component] = $component;
    }
  }

  // No pipe.
  return array();
}

/**
 * Implements hook_features_export_render().
 *
 * Dont forget to add the return line in your code.
 */
function entity_view_mode_features_export_render($module_name, $data, $export = NULL) {
  $code = array();
  $code[] = "  \$entity_view_mode = array();";
  $code[] = "";

  foreach ($data as $name) {
    if (_entity_view_mode_features_component_exists($name)) {
      list($entity_type, $machine_name) = explode(':', $name);
      $view_mode = entity_view_mode_load($entity_type, $machine_name);

      if (isset($view_mode) && is_array($view_mode)) {
        $view_mode['machine_name'] = $machine_name;

        $code[] = "  //View mode {$machine_name} ({$view_mode['label']}) for entity type {$entity_type}.";
        $code[] = "  \$view_mode = " . features_var_export($view_mode, '  ') . ";";
        $code[] = "  \$entity_view_mode['{$entity_type}']['{$machine_name}'] = \$view_mode;";
        $code[] = "";
      }
    }
  }

  $code[] = "  return \$entity_view_mode;";
  $code = implode("\n", $code);

  // The key is the default hook defined in
  // entity_view_mode_extra_featrues_api().
  return array('entity_view_mode_defaults' => $code);
}

/**
 * Implements hook_features_revert().
 */
function entity_view_mode_features_revert($module_name) {
  $entity_view_modes = module_invoke($module_name, 'entity_view_mode_defaults');

  // Loop through all entity types.
  foreach ($entity_view_modes as $entity_type => $view_modes) {

    // Loop through all view modes of a entity type.
    foreach ($view_modes as $machine_name => $view_mode) {
      entity_view_mode_save($entity_type, $view_mode);
    }

  }
}

/**
 * Helper function to check if view mode with component name exists.
 */
function _entity_view_mode_features_component_exists($component) {
  list($entity_type, $machine_name) = explode(':', $component);

  if (isset($entity_type) && isset($machine_name)) {
    $view_mode = entity_view_mode_load($entity_type, $machine_name);
    return isset($view_mode) && is_array($view_mode);
  }
  return FALSE;
}
